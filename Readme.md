README for **chef-server-ec2**
--------------------------------------------

---------------

## Building server:
 
### 1. Install things locally

1. `bundle install`

2. Open `data_bags/users/deploy.json.example` file and make sure all settings here are correct for your deployment:
    - Set password:  
      Run `openssl passwd -1 'plaintextpassword'` to generate it.
    - Copy your ssh key into the array of permitted keys:  
     You can run `pbcopy < ~/.ssh/id_rsa.pub` to copy ssh key to clipboard or `cat ~/.ssh/id_rsa.pub` to show ssh key file content.

3. Install the cookbooks on your machine: 
    `bundle exec berks install`

### 2. Amazon EC2 configuration

1. Add your ssh key to 'key pairs' - **Import Key Pair**.
2. **Launch Instance** with this key as access key. Key has to be in the same region.
3. Generate **Elastic IP** and associate it with just launched instance.

### 3. Setup remote server

1. Try to **copy your SSH** to the server:
    `ssh-copy-id user@yourserverip`
    - If everything went well go to step 2.
    - If output shows that message:  
      `All keys were skipped because they already exist on the remote system.`  
      Then it means that your ssh key is added to `authorized_keys` but you still need to run `ssh-add` with `id_rsa` file that contains your ssh key.  
      Run script:
      `sh nodes/before_setup.sh`  
      You will be prompted for SSH user and server IP.

2. Install **chef** on the remote:  
    `bundle exec knife solo prepare user@yourserverip`  
    This will generate new file in `nodes` folder: `nodes/yourserverip.json`. Open that file and make modifications to the config of the server.

3. **Build server** with:  
    `bundle exec knife solo cook user@yourserverip`

4. If you use **Ubuntu Server**, you have to install database packages at this point:  
   `sh nodes/additional_setup.sh`  
   You will be prompted for SHH user, server IP and type of database.

### Example configuration

    // data_bags/users/ubuntu.json
    {
      "id": "ubuntu",
      "password": "opensslPassword",
      "ssh_keys": [
        "youSSHKey"
      ],
      "groups": ["sysadmin"],
      "shell": "\/bin\/bash"
    }

---------------

    // nodes/yourserverip.json
    {
      "environment":"production",
      "authorization": {
        "sudo": {
          "users": ["deploy","ubuntu"]
        }
      },
      "monit": {
        "enable_emails" : false,
        "mailserver" : {
          "host" : "yourserverip",
          "port" : "port",
          "username" : "username",
          "password" : "password",
          "hostname" : "hostname"
        },
        "notify_email" : ["your@email.com"],
        "web_interface" : {
          "allow" : ["root","ubuntu"]
        }
      },
    
      "rbenv":{
        "rubies": [
          "2.2.2"
        ],
        "global" : "2.2.2",
        "gems": {
          "2.2.2" : [
            {"name":"bundler"}
          ]
        }
      },
      "vagrant" : {
        "exclusions" : [],
        "ip" : "192.168.1.32",
        "name" : "rails-postgres-redis1"
      },
      "run_list": [
        "role[server]",
        "role[nginx-server]",
        "role[rails-app]",
        "role[redis-server]",
        "role[memcached-server]"
      ],
      "automatic": {
        "ipaddress": "yourserverip"
      }
    }