#!/bin/sh
read -p 'SSH user [ubuntu]: ' user
user=${user:='ubuntu'}
read -p 'Server IP: ' ip
echo $'Options [1,2]:\n1) MySQL\n2) PostgreSQL'
read -p 'Choose database: ' option
case $option in
  1) ssh $user@$ip "sudo apt-get install libmysqlclient-dev; exit";;
  2) ssh $user@$ip "sudo apt-get install libpq-dev; exit";;
  *) echo 'Invalid option.. You need to type `1` or `2`.';;
esac
