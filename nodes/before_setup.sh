#!/bin/sh
read -p 'SSH user [ubuntu]: ' user
user=${user:='ubuntu'}
read -p 'Server IP: ' ip
ssh $user@$ip 'echo '' > ~/.ssh/id_rsa; exit'
scp ~/.ssh/id_rsa $user@$ip:~/.ssh/id_rsa
ssh $user@$ip 'chmod 600 ~/.ssh/id_rsa; ssh-agent ssh-add ~/.ssh/id_rsa; exit'
